import unittest
from app.calculator import Calculator

class TestCalculatorAdd(unittest.TestCase):
        def test_add_positive(self):
            result = Calculator.add(1,100)
            self.assertEqual(result, 101)
        def test_add_negative(self):
            result = Calculator.add(-1,-100)
            self.assertEqual(result, -101)
        def test_add_string(self):
            result = Calculator.add(1,"a")
            self.assertEqual(result, -101)
            
class TestCalculatorDivide(unittest.TestCase):
        def test_divide_positive(self):
            result = Calculator.divide(4,2)
            self.assertEqual(result, 2)
        def test_divide_byzero(self):
            result = Calculator.divide(4,0)
            self.assertIsNone(result)

class TestCalculatorSubtract(unittest.TestCase):
        def test_subtract_positive(self):
            result = Calculator.subtract(1,100)
            self.assertEqual(result, 99)
        def test_subtract_negative(self):
            result = Calculator.subtract(-1,-100)
            self.assertEqual(result, 99)
        def test_subtract_string(self):
            result = Calculator.subtract(1,"a")
            self.assertEqual(result, -101)

class TestCalculatorMultiply(unittest.TestCase):
        def test_multiply_positive(self):
            result = Calculator.multiply(1,100)
            self.assertEqual(result, 100)
        def test_multiply_negative(self):
            result = Calculator.multiply(-1,-100)
            self.assertEqual(result, 100)
        def test_multiply_string(self):
            result = Calculator.multiply(1,"a")
            self.assertEqual(result, -101)
            

class TestCalculatorModulo(unittest.TestCase):
        def test_modulo_positive(self):
            result = Calculator.modulo(1,100)
            self.assertEqual(result, 1)
        def test_modulo_negative(self):
            result = Calculator.modulo(-1,-100)
            self.assertEqual(result, -1)
        def test_modulo_string(self):
            result = Calculator.modulo(1,"a")
            self.assertEqual(result, -101)
            
class TestCalculatorElevar(unittest.TestCase):
        def test_elevar_positive(self):
            result = Calculator.elevar(1,100)
            self.assertEqual(result, 1)
        def test_elevar_negative(self):
            result = Calculator.elevar(-1,-100)
            self.assertEqual(result, -1)
        def test_elevar_string(self):
            result = Calculator.elevar(1,"a")
            self.assertEqual(result, -101)
            
class TestCalculatorFactorial(unittest.TestCase):
        def test_factorial_positive(self):
            result = Calculator.factorial(3)
            self.assertEqual(result, 6)
        def test_factorial_negative(self):
            result = Calculator.factorial(-3)
            self.assertEqual(result, 6)
        def test_factorial_string(self):
            result = Calculator.factorial("a")
            self.assertEqual(result, -101)

if __name__ == '__main__':
    unittest.main()
